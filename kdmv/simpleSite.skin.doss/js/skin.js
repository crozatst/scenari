// Skin-specific Javascript code.
if ("searchMgr" in window){
	if (scPaLib.findNode("ide:outSec")) searchMgr.fPathSchBoxParent = "ide:outSec";
	else searchMgr.fPathSchBoxParent = "ide:header";
}

// Adaptive illustration height
(function (){
	var vIllus = sc$("illus");
	var vHeaderHeight = header.offsetHeight;
	if (vIllus){
		window.resizeIllustration = function () {
			var vIllus = sc$("illus");
			var vTitleHeight = scPaLib.findNode("des:h2", sc$("content")).clientHeight
			var vIllusHeight =  Math.min(440+vTitleHeight, scPaLib.findNode("chi:img", vIllus).clientHeight);
			vIllus.style.height = vIllusHeight + "px";
			vIllus.style.top = vHeaderHeight +"px";
			sc$("content").style.marginTop = vIllusHeight-vTitleHeight+vHeaderHeight + "px";
		}
		scSiLib.addRule(vIllus, {
			onResizedDes : function(pOwnerNode, pEvent) {},
			onResizedAnc : function(pOwnerNode, pEvent) {
				if(pEvent.phase==1) resizeIllustration();
			}
		});
		scOnLoads[scOnLoads.length] = {
			onLoad : function() {
				resizeIllustration();
			}
		}
		resizeIllustration();
	}
	sc$("content").style.marginTop = vHeaderHeight + "px";
})();